import "./App.css";
import { useState, useEffect } from "react";
import _ from "lodash";
import axios from "axios";
import moment from "moment/moment";

function App() {
  const [productData, setProductData] = useState({});
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");

  useEffect(() => {
    axios
      .get("https://marketdevb2bbe.fishlog.co.id/api/cat/product/741")
      .then((res) => {
        const { data } = res;
        const { data: dataList } = data;
        setProductData(dataList);
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, []);

  const handleInputName = (e) => {
    setName(e.target.value);
  };

  const handleInputNumber = (e) => {
    setNumber(e.target.value);
  };

  if (_.isEmpty(productData)) return null;

  return (
    <div className="App">
      <header className="page__header" />
      <main className="page__main">
        <section className="page__product-section">
          <img
            alt={productData?.name}
            className="page__product-img"
            src={productData?.picture_1}
          />
          <div>
            <div className="border-b-2 border-b-gray-500 mb-3">
              <h1 className="page__heading-title mb-6">{productData?.name}</h1>
              <p className="text-lg mb-3">
                Stock Tersedia:{" "}
                <span className="font-semibold">
                  {productData?.stock?.qty} {productData?.satuan}
                </span>
              </p>
            </div>
            <table>
              <tbody>
                <tr>
                  <td>Tgl. Update</td>
                  <td>
                    :{" "}
                    {moment(productData?.stock_update_date).format(
                      "DD-MM-YYYY"
                    )}{" "}
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>&nbsp; {productData?.stock_update_duration}</td>
                </tr>
                <tr>
                  <td>Lokasi Seller</td>
                  <td>
                    : {productData?.cities?.name}, {productData?.province?.name}
                  </td>
                </tr>
                <tr>
                  <td>Min. Order Qty</td>
                  <td>
                    :{" "}
                    <span className="text-blue-600 font-semibold">
                      {productData?.minimum_order} {productData?.satuan}
                    </span>
                  </td>
                </tr>
                <tr>
                  <td>Perkiraan Hrg.</td>
                  <td>
                    :{" "}
                    <span className="text-blue-600 font-semibold">
                      {productData?.price}/{productData?.satuan}
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </section>
        <aside className="bg-blue-100 p-4 rounded">
          <h4 className="font-medium mb-4">
            Isi data dibawah ini untuk mengetahui rincian harga
          </h4>
          <form onSubmit={() => alert("submitted!")}>
            <div className="mb-4">
              <label className="block">Nama Kontak</label>
              <input
                type="text"
                placeholder="Nama"
                value={name}
                onChange={(e) => handleInputName(e)}
                required
                className="block border border-gray-400 rounded-md w-full py-3 px-2"
              ></input>
            </div>
            <div className="mb-4">
              <label className="block">Nomor Handphone/Whatsapp</label>
              <input
                type="text"
                placeholder="No. Handphone"
                value={number}
                onChange={(e) => handleInputNumber(e)}
                required
                className="block border border-gray-400 rounded-md w-full py-3 px-2"
              ></input>
            </div>
            <button
              type="submit"
              className="bg-green-400 block py-3 text-center w-full text-white rounded"
            >
              Submit
            </button>
          </form>
        </aside>
      </main>
    </div>
  );
}

export default App;
